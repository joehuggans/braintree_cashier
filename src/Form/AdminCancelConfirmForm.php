<?php

namespace Drupal\braintree_cashier\Form;

use Drupal\braintree_cashier\BillableUser;
use Drupal\braintree_cashier\Event\BraintreeCashierEvents;
use Drupal\braintree_cashier\Event\SubscriptionCanceledByUserEvent;
use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\braintree_cashier\SubscriptionService;
use Drupal\Core\Logger\LoggerChannel;

/**
 * Class CancelConfirmForm.
 */
class AdminCancelConfirmForm extends ConfirmFormBase {

  /**
   * Drupal\braintree_cashier\SubscriptionService definition.
   *
   * @var \Drupal\braintree_cashier\SubscriptionService
   */
  protected $subscriptionService;
  /**
   * Drupal\Core\Logger\LoggerChannel definition.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $loggerChannelBraintreeCashier;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The billable user service.
   *
   * @var \Drupal\braintree_cashier\BillableUser
   */
  protected $billableUser;

  /**
   * The event dispatcher.
   *
   * @var \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher
   */
  protected $eventDispatcher;

  /**
   * Constructs a new CancelConfirmForm object.
   *
   * @param \Drupal\braintree_cashier\SubscriptionService $braintree_cashier_subscription_service
   *   The subscription service.
   * @param \Drupal\Core\Logger\LoggerChannel $logger_channel_braintree_cashier
   *   The braintree_cashier logger channel.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\braintree_cashier\BillableUser $billableUser
   *   The billable user service.
   * @param \Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher $eventDispatcher
   *   The event dispatcher.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(SubscriptionService $braintree_cashier_subscription_service, LoggerChannel $logger_channel_braintree_cashier, EntityTypeManagerInterface $entityTypeManager, BillableUser $billableUser, ContainerAwareEventDispatcher $eventDispatcher) {
    $this->subscriptionService = $braintree_cashier_subscription_service;
    $this->loggerChannelBraintreeCashier = $logger_channel_braintree_cashier;
    $this->userStorage = $entityTypeManager->getStorage('user');
    $this->billableUser = $billableUser;
    $this->eventDispatcher = $eventDispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('braintree_cashier.subscription_service'),
      $container->get('logger.channel.braintree_cashier'),
      $container->get('entity_type.manager'),
      $container->get('braintree_cashier.billable_user'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    $current_path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $current_path);
    $uid = $path_args[2];
    $user = User::load($uid);
    return 'You are about to cancel the subscription for the user with the email address: '. $user->getEmail();
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return t('Are you sure you want to cancel this user\'s subscription?');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return t('Yes, cancel it.');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelText() {
    return t("Go back.");
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('<front>');
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'admin_subscription_cancel_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, User $user = NULL) {
    $form['uid'] = [
      '#type' => 'value',
      '#value' => $user->id(),
    ];

    $form['cancel_text'] = [
      '#type' => 'textfield',
      '#title' => 'Type CANCEL into this field',
    ];

    $form = parent::buildForm($form, $form_state);

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();
    if ($values['cancel_text'] != 'CANCEL') {
      $message = $this->t('You must type CANCEL into the text field.');
      $form_state->setErrorByName('cancel_text', $message);
    }

    $user = $this->userStorage->load($values['uid']);
    $subscriptions = $this->billableUser->getSubscriptions($user);
    if (empty($subscriptions)) {
      $message = $this->t('This user doesn\'t have an active subscription, please inform an administrator if you believe this to be wrong.');
      $form_state->setErrorByName('form', $message);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    /** @var \Drupal\user\Entity\User $user */
    $user = $this->userStorage->load($values['uid']);
    $subscriptions = $this->billableUser->getSubscriptions($user);
    /** @var \Drupal\braintree_cashier\Entity\BraintreeCashierSubscriptionInterface $subscription */
    foreach ($subscriptions as $subscription) {
      /** @var \Drupal\braintree_cashier\Entity\BraintreeCashierSubscriptionInterface $subscription */
      $this->subscriptionService->cancel($subscription);
      // Set the canceled at date field.
      $subscription->setCanceledAtDate(time());
      $subscription->save();
      $event = new SubscriptionCanceledByUserEvent($subscription);
      //$this->eventDispatcher->dispatch(BraintreeCashierEvents::SUBSCRIPTION_CANCELED_BY_USER, $event);
    }
    $this->messenger()->addStatus(t('User\'s subscription was cancelled.'));
  }

}
