/**
 * @file
 * Supports the signup form created with Braintree's Drop-in UI.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  var dropinInstance
  var buttonInitial
  var buttonFinal
  var nonceField
  var planSelect

  /**
   * Callback for the click event on the visible submit button.
   *
   * @param {jQuery.Event} event
   */
  function onInitialButtonClick(event) {
    event.preventDefault()

    buttonInitial.prop('disabled', true)
      .addClass('is-disabled')

    if (!dropinInstance) {
      console.log('Drop-in instance is undefined.')
      enableButtonInitial()
      return
    }

    var threeDSecureParameters = {
      email: drupalSettings.braintree_cashier.email_addr,
      amount: drupalSettings.braintree_cashier.billing_plans[planSelect.val()]
    }

    dropinInstance.requestPaymentMethod({
      threeDSecure: threeDSecureParameters
    }, function (requestPaymentMethodErr, payload) {
      if (requestPaymentMethodErr) {
        console.log(requestPaymentMethodErr)
        enableButtonInitial()
        return
      }

      if (!payload.liabilityShifted && payload.type !== 'PayPalAccount') {
        console.log('Liability did not shift')
        const messages = new Drupal.Message()
        messages.add('There was a problem with your card authentication, please try a different payment method.')
        enableButtonInitial()
        return
      }

      nonceField.val(payload.nonce)
      buttonFinal.click()

      // stopImmediatePropagation since this event handler was getting submitted
      // multiple times during automated tests.
      event.stopImmediatePropagation()

    })
  }

  /**
   * Enable the visible submit button and attach the click handler.
   */
  function enableButtonInitial() {
    buttonInitial.prop('disabled', false)
      .removeClass('is-disabled')
      .one('click', onInitialButtonClick);
  }

  /**
   * Callback for after the Dropin UI instance is created.
   *
   * @param createErr
   *   The error generated if the Dropin UI could not be created.
   * @param {object} instance
   *   The Braintree Dropin UI instance.
   *
   * @see https://braintree.github.io/braintree-web-drop-in/docs/current/Dropin.html
   */
  function onInstanceCreate(createErr, instance) {
    if (createErr) {
      console.log(createErr)
      const messages = new Drupal.Message()
      messages.add('There was a problem initialising the payment gateway, please inform an administrator.')
      return
    }
    dropinInstance = instance
    enableButtonInitial()
  }

  /**
   * Create the Braintree Dropin UI.
   *
   * @type {{attach: Drupal.behaviors.new_subsription.attach}}
   */
  Drupal.behaviors.new_subsription = {
    attach: function (context, settings) {

      $('body', context).once('instantiate-dropin').each(function() {

        buttonInitial = $('#submit-button', context)
        buttonFinal = $('#final-submit', context)
        nonceField = $('#payment-method-nonce', context)
        planSelect = $('#edit-plan-entity-id', context)

        var createParams = {
          authorization: drupalSettings.braintree_cashier.authorization,
          container: '#dropin-container',
          threeDSecure: true
        };

        if (drupalSettings.braintree_cashier.acceptPaypal) {
          createParams.paypal = {
            flow: 'vault'
          }
        }

        braintree.dropin.create(createParams, onInstanceCreate)
      });
    }
  };

})(jQuery, Drupal, drupalSettings);
