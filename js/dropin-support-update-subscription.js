/**
 * @file
 * Supports the signup form created with Braintree's Drop-in UI.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  var clientInstance
  var threeDSInstance
  var dropinInstance
  var buttonInitial
  var buttonFinal
  var nonceField

  /**
   * Callback for the click event on the visible submit button.
   *
   * @param {jQuery.Event} event
   */
  function onInitialButtonClick(event) {
    event.preventDefault()

    buttonInitial.prop('disabled', true)
      .addClass('is-disabled')

    if (!dropinInstance) {
      console.log('Drop-in instance is undefined.')
      enableButtonInitial()
      return
    }

    dropinInstance.requestPaymentMethod(function (requestPaymentMethodErr, payload) {
      if (requestPaymentMethodErr) {
        console.log(requestPaymentMethodErr)
        enableButtonInitial()
        return
      }

      if (payload.type !== 'PayPalAccount') {

        threeDSInstance.verifyCard({
          amount: drupalSettings.braintree_cashier.price,
          nonce: payload.nonce,
          bin: payload.details.bin,
          onLookupComplete: function (data, next) {
            // use `data` here, then call `next()`
            next();
          }
        }, function (verifyError, payload) {
          if (verifyError) {
            if (verifyError.code === 'THREEDS_VERIFY_CARD_CANCELED_BY_MERCHANT ') {
              // flow was canceled by merchant, 3ds info can be found in the payload
              // for cancelVerifyCard
            }
          }

          if (!payload.liabilityShifted) {
            console.log('Liability did not shift')
            const messages = new Drupal.Message()
            messages.add('There was a problem with your card authentication, please try a different payment method.')
            enableButtonInitial()
            return
          }

          nonceField.val(payload.nonce)
          buttonFinal.click()
        });
      } else {
        nonceField.val(payload.nonce)
        buttonFinal.click()
      }

    });

  }


  /**
   * Enable the visible submit button and attach the click handler.
   */
  function enableButtonInitial() {
    buttonInitial.prop('disabled', false)
      .removeClass('is-disabled')
      .one('click', onInitialButtonClick)
  }

  /**
   * Callback for after the Braintree client instance is created.
   */
  function onClientInstanceCreate(createErr, instance) {
    if (createErr) {
      console.log(createErr)
      const messages = new Drupal.Message()
      messages.add('There was a problem initialising the payment gateway, please inform an administrator.')
      return
    }
    clientInstance = instance
    braintree.threeDSecure.create({
      version: 2,
      client: clientInstance
    }, function (threeDSecureErr, threeDSecureInstance) {
      threeDSInstance = threeDSecureInstance
    })
  }

  /**
   * Callback for after the Dropin UI instance is created.
   *
   * @param createErr
   *   The error generated if the Dropin UI could not be created.
   * @param {object} instance
   *   The Braintree Dropin UI instance.
   *
   * @see https://braintree.github.io/braintree-web-drop-in/docs/current/Dropin.html
   */
  function onDropinInstanceCreate(createErr, instance) {
    if (createErr) {
      console.log(createErr)
      const messages = new Drupal.Message()
      messages.add('There was a problem initialising the payment gateway, please inform an administrator.')
      return
    }
    dropinInstance = instance
    enableButtonInitial()
  }

  /**
   * Create the Braintree Dropin UI.
   *
   * @type {{attach: Drupal.behaviors.update_subscription.attach}}
   */
  Drupal.behaviors.update_subscription = {
    attach: function (context, settings) {

      $('body', context).once('instantiate-dropin').each(function () {

        buttonInitial = $('#submit-button', context)
        buttonFinal = $('#final-submit', context)
        nonceField = $('#payment-method-nonce', context)

        var createParamsClient = {
          authorization: drupalSettings.braintree_cashier.authorization,
        };

        var createParamsDropin = {
          authorization: drupalSettings.braintree_cashier.authorization,
          container: '#dropin-container'
        };

        if (drupalSettings.braintree_cashier.acceptPaypal) {
          createParamsDropin.paypal = {
            flow: 'vault'
          }
        }

        braintree.client.create(createParamsClient, onClientInstanceCreate)
        braintree.dropin.create(createParamsDropin, onDropinInstanceCreate)
      });
    }
  }

})(jQuery, Drupal, drupalSettings)
